use std::path::Path;
use std::fs;
use std::process;

fn main() {
    let result = Path::new("Cargo.toml").exists();
    println!("{:?}", result);

    let content = match fs::read_to_string("Cargo.toml") {
        Ok(c) => c,
        Err(_) => {
            println!("error: could not read file");
            process::exit(1);
        }
    };

    println!("{:}", content);
}
